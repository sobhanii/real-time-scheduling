import operator
import numpy as np

food_menu = []
orderList = []

class Food_type():

    name = ""
    cookingTime = 0
    deadLine = 0
    period = 0
    waiting_time=0
    def __init__(self,name, cookingTime, deadLine, period):
        self.name = name
        self.cookingTime = cookingTime
        self.deadLine = deadLine
        self.period = period

class Order():

    cookingtimeRemained = 0
    real_deadline = 0
    arrive = 0
    laxity = 0
    def __init__(self,type,arrive):
        self.type = type
        self.arrive = arrive
        self.cookingtimeRemained = type.cookingTime
        self.real_deadline = arrive + self.type.deadLine
        orderList.append(self)


#getting input from console
number_of_orders = input()
for i in range(int(number_of_orders)):
    data = input()
    name = data.split(" ")[0]
    cookingtime = int(data.split(" ")[1])
    deadline = int(data.split(" ")[2])
    period = int(data.split(" ")[3])
    food_type = Food_type(name,cookingtime,deadline,period)
    food_menu.append(food_type)

#tc:
# 3
# food1 1 5 6
# food2 2 4 5
# food3 4 8 10

#define total runtime with least common divisor
def define_total_runTime():
    periods_list = []
    for food in food_menu:
        periods_list.append(food.period)
    runtime = int(np.lcm.reduce(periods_list))
    return runtime

#function for waiting time
def add_waits(current_order):
    for order in orderList:
        if order != current_order:
            order.type.waiting_time +=1

def EDF_next_order(current_order):
    orderList_sorted = sorted(orderList, key=operator.attrgetter('real_deadline'))
    if len(orderList) == 0:
        return

    if(current_order!= None and current_order.real_deadline == orderList_sorted[0].real_deadline):
        return current_order

    min_order = orderList_sorted[0]
    min_CT = orderList_sorted[0].cookingtimeRemained
    i=1
    while i<len(orderList_sorted) and orderList_sorted[i].real_deadline == min_order.real_deadline:
        if orderList_sorted[i].cookingtimeRemained < min_CT:
            min_CT = orderList_sorted[i].cookingtimeRemained
            min_order = orderList_sorted[i]
        i+=1
    return min_order


def EarliestDeadlineFirst():
    runtime = define_total_runTime()
    idletimes = 0
    current_order= None

    for t in range(0,runtime):
        for food in food_menu:
            if t%(food.period) == 0 :
                Order(food,t)
        current_order = EDF_next_order(current_order)
        add_waits(current_order)
        if current_order == None:
            idletimes = idletimes + 1
            print("from {} to {} {}".format(t, t + 1, 'Idle'))
        else:
            print("from {} to {} {}".format(t, t+ 1, current_order.type.name))
            # updating cooking time
            current_order.cookingtimeRemained = current_order.cookingtimeRemained - 1
            if current_order.cookingtimeRemained == 0:
                orderList.remove(current_order)
                current_order=None
        for order in orderList:
            if t+1 == (order.arrive + order.type.deadLine):
                print("{} deadline missed".format(order.type.name))
    print("idle times {}".format(idletimes))
    for food in food_menu:
        print("{} waiting time = {}".format(food.name, food.waiting_time))

#EarliestDeadlineFirst()


#return the prior order to prepare
def define_prior_order(current_order):
    if  len(orderList) != 0:
        if(current_order == None):
            periorOrder = orderList[0]
            lessperior = orderList[0].type.period
        else:
            lessperior = current_order.type.period
            periorOrder = current_order
    if len(orderList) == 0:
        return
    else:
        for order in orderList:
                if lessperior > order.type.period:
                    periorOrder = order
                    lessperior = order.type.period
                elif lessperior == order.type.period:
                    if periorOrder==current_order:
                        pass
                    elif periorOrder.cookingtimeRemained > order.cookingtimeRemained:
                        periorOrder=order
                        lessperior = order.type.period
        return periorOrder


def is_possible():
    runtime = define_total_runTime()
    needed = 0
    for food in food_menu:
        times = runtime/food.period
        needed = needed + (food.cookingTime * times)
    if needed > runtime:
        print("cant handle orders.")
        return False
    return True


#rate monotinic scheduling
def rate_monotonic():
    idletimes = 0
    runtime = define_total_runTime()
    if not is_possible():
        return

    priorOrder = None
    for t in range(0,runtime):
        for food in food_menu:
            if t%(food.period) == 0 :
                Order(food,t)
        #check if period finished without order finish
        priorOrder = define_prior_order(priorOrder)
        add_waits(priorOrder)
        if priorOrder == None:
            idletimes = idletimes + 1
            print("from {} to {} {}".format(t, t + 1, 'Idle'))
        else:
            print("from {} to {} {}".format(t, t+ 1, priorOrder.type.name))
            # updating cooking time
            priorOrder.cookingtimeRemained = priorOrder.cookingtimeRemained - 1
            if priorOrder.cookingtimeRemained == 0:
                orderList.remove(priorOrder)
                priorOrder=None
        for order in orderList:
            if t+1 == (order.arrive + order.type.deadLine):
                print("{} deadline missed".format(order.type.name))
    print("idle times {}".format(idletimes))
    for food in food_menu:
        print("{} waiting time = {}".format(food.name, food.waiting_time))

#rate_monotonic()


def LLF_next_order(t,current_order):
    for order in orderList:
        order.laxity = order.real_deadline - t - order.cookingtimeRemained
    orderList_sorted = sorted(orderList, key=operator.attrgetter('laxity'))
    if len(orderList) == 0:
        return
    if (current_order != None and current_order.laxity == orderList_sorted[0].laxity):
        return current_order
    min_order = orderList_sorted[0]
    min_CT = orderList_sorted[0].cookingtimeRemained
    i = 1
    while i < len(orderList_sorted) and orderList_sorted[i].laxity == min_order.laxity:
        if orderList_sorted[i].cookingtimeRemained < min_CT:
            min_CT = orderList_sorted[i].cookingtimeRemained
            min_order = orderList_sorted[i]
        i += 1
    return min_order

def least_laxity_first():
    runtime = define_total_runTime()
    idletimes = 0
    if not is_possible():
        return
    current_order = None
    for t in range(0, runtime):
        for food in food_menu:
            if t % (food.period) == 0:
                Order(food, t)
        current_order = LLF_next_order(t,current_order)
        add_waits(current_order)
        if current_order == None:
            idletimes = idletimes + 1
            print("from {} to {} {}".format(t, t + 1, 'Idle'))
        else:
            print("from {} to {} {}".format(t, t + 1, current_order.type.name))
            # updating cooking time
            current_order.cookingtimeRemained = current_order.cookingtimeRemained - 1
            if current_order.cookingtimeRemained == 0:
                orderList.remove(current_order)
                current_order = None
        for order in orderList:
            if t + 1 == (order.arrive + order.type.deadLine):
                print("{} deadline missed".format(order.type.name))

    print("idle times {}".format(idletimes))
    for food in food_menu:
        print("{} waiting time = {}".format(food.name, food.waiting_time))

least_laxity_first()


